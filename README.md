# ZeroTier VPN - Manager

Helper to manage ZeroTier connections.

## Samples

```
# add a network to networks file
./zerotiervpn-manager.sh add --networkid 830eeee632b7f0bb --network kungfu 
./zerotiervpn-manager.sh add --networkid 830eeee632b7f0bb --network kungfu --setroute --lancidr 192.168.59.0/24 --gatewayip 192.168.136.184

# delete a network to networks file
./zerotiervpn-manager.sh delete --network kungfu

# join and leave networks
./zerotiervpn-manager.sh join --network kungfu
./zerotiervpn-manager.sh leave --network kungfu
```

## What's this?

When you join a ZeroTier Network, it can be permanent or temparary. Meaning, if you join your work VPN, maybe you want to disconnect it when you take a rest to watch a movie.

To join or leave ZeroTier Networks you need to remember the ID.... not easy.

This tool allows you to have a file with your networks, assigning custom easy names, and then join or leave them using these names.

Plus, sometimes you need to add a default route to your iptables. E.g. I created a VPN on my home, I have a Proxmox with various containers, one of them is a gateway, then if I want to reach the others containers from the VPN I'll need to add an iptables route hitting my LAN's CIDR. 

This tool creates and deletes the needed routes on a GNU/Linux with iptables.

## The database file

Networks are stored on a file on the same path the script is run from. This is the structure:

```
networkidentifier,networkid,setroute,lancidr,gatewayip
```

E.g.:

```
work,83eeea0632b7f0bb,1,192.163.39.0/24,192.163.196.444
```

lancidr is the ip range of the LAN you are trying to reach
gatewayip is the one ZeroTier has assigned to it

# Argbash

This script was made using [Argbash](argbash.io/), so if you need to modify it, modify the _m4_ file and then create the _sh_ one with this command:

```
argbash zerotiervpn-manager.m4 -o zerotiervpn-manager.sh
```

