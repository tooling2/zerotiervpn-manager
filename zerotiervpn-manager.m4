#!/bin/bash

# m4_ignore(
echo "This is just a script template, not the script (yet) - pass it to 'argbash' to fix this." >&2
exit 11  #)Created by argbash-init v2.8.1
# ARG_OPTIONAL_BOOLEAN([quiet])
# ARG_POSITIONAL_SINGLE([verb],[What to do, one of join, leave, show (show available networks), status (status of connections),  add|delete|modify (add, delete, or modify a new network); default is show],[show])
# ARG_DEFAULTS_POS
# ARG_OPTIONAL_SINGLE([network], n, [The ZeroTier Network custom name, e.g. work, if verb is add this name will be set ])
# ARG_OPTIONAL_SINGLE([networkid], , [If verb is add, The ZeroTier Network id to set])
# ARG_OPTIONAL_BOOLEAN([setroute], , [If verb is add, whether or not is needed to set route to VPN CIDR])
# ARG_OPTIONAL_SINGLE([lancidr], , [If verb is add and setroute is set, the LAN CIDR to set in the route])
# ARG_OPTIONAL_SINGLE([gatewayip], , [If verb is add and setroute is set, the gateway (the one provided by ZeroTier) ip to set in the route])
# ARG_OPTIONAL_BOOLEAN([join], j, [If verb is add, once added will join the network ])
# ARG_OPTIONAL_BOOLEAN([wide], w, [If verb is show, will display more data ])
# ARG_HELP([Helper to manage ZeroTier connections.\n \nSamples:\n ./zerotiervpn-manager.sh add --networkid 830eeee632b7f0bb --network kungfu \n ./zerotiervpn-manager.sh add --networkid 830eeee632b7f0bb --network kungfu --setroute --lancidr 192.168.59.0/24 --gatewayip 192.168.136.184\n ./zerotiervpn-manager.sh delete --network kungfu\n ./zerotiervpn-manager.sh join --network kungfu\n ./zerotiervpn-manager.sh leave --network kungfu\n \n])
# ARGBASH_GO

# [ <-- needed because of Argbash

# DB FILE
# Sample content:
# networkidentifier,networkid,setroute,lancidr,gatewayip
# e.g.:
# work,83eeea0632b7f0bb,1,192.168.33.0/24,192.168.196.444
#
# lancidr is the ip range of the LAN you are trying to reach
# gatewayip is the one ZeroTier has assigned to it
DBFILE="./zerotier-networks.csv"

NETWORK_FOUND=0
NETWORK_CONNECTED=0
NETWORK_ID=""
NETWORK_SETROUTE=0
NETWORK_VPN_CIDR=""
NETWORK_GATEWAY=""

MAX_ATTEMPS_FOR_WAIT=10
DELAY_TO_WAIT=5

say () {
	if [ "$_arg_quiet" = "off" ];
	then
		printf "%s\\n" "$1"
	fi
}

set_network () {
    if [ "$_arg_network" != "" ];
    then
        get_network
    fi
    if [ "$NETWORK_SETROUTE" == "1" ];
    then
        if [ "$NETWORK_VPN_CIDR" == "" ] || [ "$NETWORK_GATEWAY" == "" ];
        then
            die "Can't set route with no data!" 1
        fi
    fi
    check_data
    if [ "$NETWORK_ID" == "" ];
    then
        die "Bad network id" 1
    fi    
}

get_network () {
    local result=$(grep "^"$_arg_network"," $DBFILE)
    if [ $? -eq 0 ] && [ "$result" != "" ];
    then
        NETWORK_FOUND=1
        say "fodun $result"
        while IFS=',' read i id route cidr gw
        do
            say "getting $id"
            NETWORK_ID=$id
            NETWORK_SETROUTE=$route
            NETWORK_VPN_CIDR=$cidr
            NETWORK_GATEWAY=$gw
        done <<< "$result"
    else
        die "Network $_arg_network not found, try to add it first" 1
    fi
}

add () {
    local result=$(grep "^"$_arg_network"," $DBFILE)
    if [ $? -eq 0 ] && [ "$result" == "" ];
    then
        local newline=""
        if [ "$_arg_network" != "" ] && [ "$_arg_networkid" != "" ];
        then
            newline=$_arg_network","$_arg_networkid","
            if [ "$_arg_setroute" == "on" ];
            then
                if [ "$_arg_lancidr" != "" ] || [ "$_arg_gatewayip" != "" ];
                then
                    newline=$newline"1,"$_arg_lancidr","$_arg_gatewayip
                else
                    die "If setroute is set, lancidr and gatewayip must be set as well" 1
                fi
            else
                newline=$newline"0,,"
            fi
        fi
        echo $newline >> $DBFILE
    else
        die "Network $_arg_network already exists, try to delete it first or modify" 1
    fi
}

delete () {
    local result=$(grep "^"$_arg_network"," $DBFILE)
    if [ $? -eq 0 ] && [ "$result" != "" ];
    then
        sed -i '/^'$_arg_network',/d' $DBFILE
    else
        die "Network $_arg_network does not exist" 1
    fi
}

modify () {
	_PRINT_HELP="yes"
    die "Modify still not implemented" 1
}

join () {
    get_status $NETWORK_ID
    if [ $NETWORK_CONNECTED -eq 0 ];
    then
        say "    joining $_arg_network ($NETWORK_ID)"
        sudo zerotier-cli join $NETWORK_ID
    else
        die "Already connected" 1
    fi
}

leave () {
    get_status $NETWORK_ID
    if [ $NETWORK_CONNECTED -eq 1 ];
    then
        say "    leaving $_arg_network ($NETWORK_ID)"
        sudo zerotier-cli leave $NETWORK_ID
    else
        die "Not connected" 1
    fi
}

add_route () {
    sudo zerotier-cli set $NETWORK_ID allowDefault=1 > /dev/null
    dev=$(sudo zerotier-cli get $NETWORK_ID portDeviceName)
    if [ "$dev" != "" ];
    then
        if [[ ! "$dev" =~ ^[a-zA-Z0-9]+$ ]];
        then
            die "Can't understand device, found: $dev" 1
        fi
    else
        die "Can't find device for network $_arg_network $NETWORK_ID" 1
    fi
    
    sudo route add -net $NETWORK_VPN_CIDR gw $NETWORK_GATEWAY $dev
}

delete_route () {
    dev=$(sudo zerotier-cli get $NETWORK_ID portDeviceName)
    if [ "$dev" != "" ];
    then
        if [[ ! "$dev" =~ ^[a-zA-Z0-9]+$ ]];
        then
            die "Can't understand device, found: $dev" 1
        fi
    else
        die "Can't find device for network $_arg_network $NETWORK_ID" 1
    fi
    
    sudo route del -net $NETWORK_VPN_CIDR gw $NETWORK_GATEWAY $dev

}

wait_for_network () {
    say "   ...waiting for network $_arg_network ($NETWORK_ID)..."
    get_status $NETWORK_ID
    local attemps=0
    while [ $NETWORK_CONNECTED -ne 1 ] && [ $attemps -lt $MAX_ATTEMPS_FOR_WAIT ];
    do
        attemps=$((attemps + 1))
        sleep $DELAY_TO_WAIT
        say "   ...waiting for network $_arg_network ($NETWORK_ID)i, attemp $attemps..."
        get_status $NETWORK_ID
    done
}

show () {
    if [ "$_arg_wide" == "off" ];
    then
        say " "
        say "Network, ID"
        cat $DBFILE | awk -F ',' '{print $1"," $2}'
        say " "
    else
        say " "
        say "Network, ID, SetRoute, CIDR, GW"
        cat $DBFILE | awk -F ',' '{print $1","$2","$3","$4","$5}'
        say " "
    fi
}

get_status () {
    if [ "$1" != "" ];
    then
        local cmd="sudo zerotier-cli listnetworks -j | jq '.[] | select( .id | contains(\"$1\")) | .status'"
        local status=$(eval $cmd)
        if [ $? -eq 0 ];
        then
            if [ "$status" = "" ];
            then
                NETWORK_CONNECTED=0
            elif [[ "$status" =~ "OK" ]];
            then
                NETWORK_CONNECTED=1
            else
                NETWORK_CONNECTED=2
            fi
        else
            die "A problem occurred!" 1
        fi
    else
        for i in $(sudo zerotier-cli listnetworks -j | jq '.[] | "\(.id),\(.status)"')
        do 
            i=$(echo $i | sed 's/"//g')
            local nid=$(echo $i | awk -F ',' '{print $1}')
            local nstatus=$(echo $i | awk -F ',' '{print $2}')
            local name=$(cat $DBFILE | grep $nid | awk -F ',' '{print $1}')
            if [ "$name" == "" ];
            then
                name="<unmanaged>"
            fi
            echo $name" ("$nid"): "$nstatus
        done
    fi
}

check_dbfile () {
    if [ ! -f $DBFILE ];
    then
        touch $DBFILE
        if [ $? -ne 0 ];
        then
            die "Can't create db file $DBFILE" 1
        fi
    fi
}

check_parameters () {

    if [ "$_arg_network" != "" ];
    then
        if [[ ! "$_arg_network" =~ ^[a-zA-Z0-9]+$ ]];
        then
            die "Network name only can contain a-zA-Z0-9" 1
        fi
    fi
    if [ "$_arg_networkid" != "" ];
    then
        if [[ ! "$_arg_networkid" =~ ^[a-zA-Z0-9]+$ ]];
        then
            die "Network id only can contain a-zA-Z0-9" 1
        fi
    fi
    if [ "$_arg_lancidr" != "" ];
    then
        if [[ ! "$_arg_lancidr" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\/[0-9]+$ ]];
        then
            die "LAN CIDR only can be something like 99.99.99.99/24 " 1
        fi
    fi
    if [ "$_arg_gatewayip" != "" ];
    then
        if [[ ! "$_arg_gatewayip" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]];
        then
            die "Gateway IP only can be something like 99.99.99.99" 1
        fi
    fi
}

check_data () {
    if [ "$NETWORK_VPN_CIDR" != "" ];
    then
        if [[ ! "$NETWORK_VPN_CIDR" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\/[0-9]+$ ]];
        then
            die "LAN CIDR only can be something like 99.99.99.99/24 " 1
        fi
    fi
    if [ "$NETWORK_GATEWAY" != "" ];
    then
        if [[ ! "$NETWORK_GATEWAY" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]];
        then
            die "Gateway IP only can be something like 99.99.99.99" 1
        fi
    fi
}

say "* ********************************** *"
say "* ZeroTier VPN for GNU/Linux         *"
say "* ********************************** *"
say " "

say "Checking dbfile $DBFILE..."
check_dbfile
say "Checking Parameters..."
check_parameters

say "* ---------------- *"
say "* Taking action    *"
say " "

case $_arg_verb in
	join)
        say "Joining network $_arg_network..."
        set_network
		join
        wait_for_network
        if [ "$NETWORK_SETROUTE" == "1" ];
        then
            add_route
        fi
		;;
	leave)
        say "Leaving network $_arg_network..."
        if [ "$NETWORK_SETROUTE" == "1" ];
        then
            delete_route
        fi
        set_network
		leave
		;;
	show)
        say "Showing networks..."
		show
		;;
	add)
        say "Adding network $_arg_network..."
		add
		;;
    delete)
        say "Deleting network $_arg_network..."
        delete
        ;;
    modify)
        say "Modifying network $_arg_network..."
        modify
        ;;
    status)
        say "Status..."
        get_status
        ;;
	*)
		_PRINT_HELP="yes"
		die "Verb $_arg_verb does not exist!" 1
		;;
esac

say "Process finished."
# ] <-- needed because of Argbash
